@echo off
cd .\Dissertation
del *.aux *.bbl *.bcf *.blg *.log *.pdf *.run.xml *.backup *.toc *-blx.bib
cd .\..\ResearchReport
del *.aux *.bbl *.bcf *.blg *.log *.pdf *.run.xml *.backup *.toc *-blx.bib
